module Main where

import           Protolude
import           System.IO (hSetEncoding, utf8)

import           Distribution.Simple.Configure

import           Options.Generic

import           Parochial.HaddockGenerator
import           Parochial.HoogleGenerator
import           Parochial.Options
import           Parochial.Types


main :: IO ()
main = do
  hSetEncoding stdout utf8
  parseCmdOptions >>= subCmd


-- Choose between building the Hoogle database or the Haddocks
subCmd :: Config Unwrapped -> IO ()
subCmd c = case c of
  (Haddock t p s) -> build t p s createSymLinkFarm
  (Hoogle t p s)  -> build t p s buildHoogleDB


-- Sort the options out and call the passed in function
build :: Maybe Target -> Maybe Text -> Maybe FilePath -> (Target -> [Pkg] -> IO ()) -> IO ()
build t p s f = do
  setupConfig <- getState s
  targetPath <- getAndMkTargetPath p t
  getConfigStateFile setupConfig >>= installedHaddocks >>= f targetPath
