module Parochial.HoogleGenerator (
    buildHoogleDB
  ) where


import           Protolude
import qualified Data.String as S
import qualified Hoogle as H

import           System.FilePath.Posix (addExtension)

import           Parochial.Types


-- Generate the Hoogle options to be passed to @hoogle@.
-- This is a bit yucky as it uses @hoogle@ which appears to be
-- a convenience function for the command line tool. I did look at not
-- using this wrapper but it's a bit intimidating!
hoogleGenOptions :: Target -> [FilePath] -> [S.String]
hoogleGenOptions t ps = generateCommand <> locals ps
  where
    generateCommand = ["generate", "--database=" <> addExtension t "hoo"]
    locals = map ("--local=" <>)


-- | Build the hoogle database. This extracts the paths from all the packages,
--   checks they exist and passes them to the hoogle command.
buildHoogleDB :: Target -> [Pkg] -> IO ()
buildHoogleDB t = H.hoogle . hoogleGenOptions t . extractPaths
  where
    extractPaths = map snd


