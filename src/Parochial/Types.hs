module Parochial.Types (
    Pkg
  , Target
  ) where

import           Protolude
import           Distribution.Types.PackageId


type Pkg = (PackageIdentifier, FilePath)
type Target = FilePath
